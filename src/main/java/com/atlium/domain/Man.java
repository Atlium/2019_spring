package com.atlium.domain;

import com.atlium.dto.ManDto;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "mans")
public class Man {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthday;

    public Man() {
    }

    public Man(ManDto manDto) {
        this.id = manDto.getId();
        this.firstName = manDto.getFirstName();
        this.lastName = manDto.getLastName();
        this.birthday = manDto.getBirthday();
    }

    public Man(Long id) {
        this.id = id;
    }

    public Man(String firstName, String lastName, Date birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Man)) return false;
        Man man = (Man) o;
        return Objects.equals(getId(), man.getId()) &&
                Objects.equals(getFirstName(), man.getFirstName()) &&
                Objects.equals(getLastName(), man.getLastName()) &&
                Objects.equals(getBirthday(), man.getBirthday());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getFirstName(), getLastName(), getBirthday());
    }

    @Override
    public String toString() {
        return "Man{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
