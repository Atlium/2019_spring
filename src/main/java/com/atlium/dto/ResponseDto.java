package com.atlium.dto;

import com.atlium.model.ResponseStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseDto<D extends Serializable> implements Serializable {
    private static final long serialVersionUID = -7857979170337567930L;
    private ResponseStatus responseStatus;
    private String message;
    private D result;
    private List<D> resultList = new ArrayList<>();

    public ResponseDto() {
    }

    public ResponseDto(ResponseStatus responseStatus, String message) {
        this.responseStatus = responseStatus;
        this.message = message;
    }

    public ResponseDto(ResponseStatus responseStatus, String message, D result) {
        this.responseStatus = responseStatus;
        this.message = message;
        this.result = result;
    }

    public ResponseDto(ResponseStatus responseStatus, String message, List<D> resultList) {
        this.responseStatus = responseStatus;
        this.message = message;
        this.resultList = resultList;
    }

    public ResponseDto(ResponseStatus responseStatus, String message, D result, List<D> resultList) {
        this.responseStatus = responseStatus;
        this.message = message;
        this.result = result;
        this.resultList = resultList;
    }

    public ResponseDto(D result) {
        this.responseStatus = ResponseStatus.SUCCESS;
        this.message = null;
        this.result = result;
    }

    public ResponseDto(List<D> resultList) {
        this.responseStatus = ResponseStatus.SUCCESS;
        this.message = null;
        this.resultList = resultList;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public D getResult() {
        return result;
    }

    public void setResult(D result) {
        this.result = result;
    }

    public List<D> getResultList() {
        return resultList;
    }

    public void setResultList(List<D> resultList) {
        this.resultList = resultList;
    }
}
