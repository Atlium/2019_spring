package com.atlium.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaginatedDto<D extends Serializable> implements Serializable {
    private static final long serialVersionUID = -5523829612191013360L;
    private List<D> list = new ArrayList<>();
    private int allCount;

    public PaginatedDto() {
    }

    public PaginatedDto(List<D> list, int allCount) {
        this.list = list;
        this.allCount = allCount;
    }

    public List<D> getList() {
        return list;
    }

    public void setList(List<D> list) {
        this.list = list;
    }

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }
}
