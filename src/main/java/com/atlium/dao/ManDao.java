package com.atlium.dao;

import com.atlium.domain.Man;

public interface ManDao extends CrudDao<Man, Long> {
}
