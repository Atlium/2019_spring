package com.atlium.dao;

import java.util.List;

public interface CrudDao<E, K> {
    E save(E entity);

    void remove(K id);

    E update(E entity);

    E find(K id);

    List<E> findAll();

    Long count();

    List<E> findAllPaging(int page, int pageSize);
}
