package com.atlium.dao.jpa;

import com.atlium.dao.CrudDao;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractCrudDao<E, K> implements CrudDao<E, K> {

    private static final Logger log = Logger.getLogger(AbstractCrudDao.class);

    @Override
    public E save(E entity) {
        getEntityManager().persist(entity);
        log.info("saved new entity: " + entity);
        return entity;
    }

    @Override
    public void remove(K id) {
        E entityToRemove = find(id);
        getEntityManager().remove(entityToRemove);
        log.info("removed entity: " + entityToRemove);
    }

    @Override
    public E update(E entity) {
        E updatedEntity = getEntityManager().merge(entity);
        log.info("updated entity: " + updatedEntity);
        return updatedEntity;
    }

    @Override
    public E find(K id) {
        E foundedEntity = getEntityManager().find(getEntityClass(), id);
        log.info("entity successfully founded: " + foundedEntity);
        return foundedEntity;
    }

    @Override
    public List<E> findAll() {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = criteriaBuilder.createQuery(getEntityClass());
        Root<E> variableRoot = query.from(getEntityClass());
        query.select(variableRoot);
        List<E> resultList = entityManager.createQuery(query).getResultList();
        log.info("founded entities count: " + resultList.size());
        return resultList;
    }

    @Override
    public Long count() {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        query.select(criteriaBuilder.count(query.from(getEntityClass())));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public List<E> findAllPaging(int page, int pageSize) {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = criteriaBuilder.createQuery(getEntityClass());
        Root<E> variableRoot = query.from(getEntityClass());
        query.select(variableRoot);
        List<E> resultList = entityManager.createQuery(query).setFirstResult(pageSize * page).setMaxResults(pageSize).getResultList();
        log.info("founded entities count: " + resultList.size());
        return resultList;
    }

    protected abstract EntityManager getEntityManager();

    protected abstract Class<E> getEntityClass();
}
