package com.atlium.dao.jpa;

import com.atlium.dao.ManDao;
import com.atlium.domain.Man;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ManDaoImpl extends AbstractCrudDao<Man, Long> implements ManDao {

    private static final Logger log = Logger.getLogger(ManDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    protected Class<Man> getEntityClass() {
        return Man.class;
    }
}
