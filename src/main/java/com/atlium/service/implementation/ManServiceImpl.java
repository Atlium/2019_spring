package com.atlium.service.implementation;

import com.atlium.dao.ManDao;
import com.atlium.domain.Man;
import com.atlium.dto.ManDto;
import com.atlium.service.ManService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ManServiceImpl implements ManService {

    private static final Logger log = Logger.getLogger(ManServiceImpl.class);
    private ManDao manDao;

    @Autowired
    public ManServiceImpl(ManDao manDao) {
        this.manDao = manDao;
    }

    @Override
    public ManDto createMan(ManDto man) {
        return new ManDto(manDao.save(new Man(man)));
    }

    @Override
    public void removeMan(Long id) {
        manDao.remove(id);
    }

    @Override
    public ManDto updateMan(ManDto man) {
        Man manFromDb = manDao.find(man.getId());
        manFromDb.setFirstName(man.getFirstName());
        manFromDb.setLastName(man.getLastName());
        manFromDb.setBirthday(man.getBirthday());
        return new ManDto(manDao.update(manFromDb));
    }

    @Override
    public ManDto updateManParticularly(ManDto man) {
        Man manFromDb = manDao.find(man.getId());
        if (man.getFirstName() != null) manFromDb.setFirstName(man.getFirstName());
        if (man.getLastName() != null) manFromDb.setLastName(man.getLastName());
        if (man.getBirthday() != null) manFromDb.setBirthday(man.getBirthday());
        return new ManDto(manDao.update(manFromDb));
    }

    @Override
    public List<ManDto> findAllMans() {
        return manDao.findAll().stream().map(ManDto::new).collect(Collectors.toList());
    }

    @Override
    public ManDto findById(Long id) {
        return new ManDto(manDao.find(id));
    }

}
