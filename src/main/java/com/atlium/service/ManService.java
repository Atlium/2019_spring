package com.atlium.service;

import com.atlium.dto.ManDto;

import java.util.List;

public interface ManService {
    ManDto createMan(ManDto man);

    void removeMan(Long id);

    ManDto updateMan(ManDto man);

    ManDto updateManParticularly(ManDto man);

    List<ManDto> findAllMans();

    ManDto findById(Long id);
}
