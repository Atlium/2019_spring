package com.atlium.controller;

import com.atlium.dto.ManDto;
import com.atlium.dto.ResponseDto;
import com.atlium.model.ResponseStatus;
import com.atlium.service.ManService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mans")
public class ManController {

    private static final Logger log = Logger.getLogger(ManController.class);
    private ManService manService;
    public static final String MAN_REMOVED_MESSAGE = "Man was deleted successfully";

    @Autowired
    public ManController(ManService manService) {
        this.manService = manService;
    }

    @PostMapping
    public ResponseDto<ManDto> createMan(@RequestBody ManDto man) {
        log.info("Try to create man: " + man);
        ManDto manDto = manService.createMan(man);
        log.info("Created man: " + manDto);
        return new ResponseDto<>(manDto);
    }

    @GetMapping
    public ResponseDto<ManDto> findAllMans() {
        log.info("Find all mans");
        List<ManDto> list = manService.findAllMans();
        log.info("Founded mans: " + list);
        return new ResponseDto<>(list);
    }

    @GetMapping("/{id}")
    public ResponseDto<ManDto> findManById(@PathVariable Long id) {
        log.info("Find man by id: " + id);
        ManDto manDto = manService.findById(id);
        log.info("Founded man: " + manDto);
        return new ResponseDto<>(manDto);
    }

    @PutMapping("/{id}")
    public ResponseDto<ManDto> updateMan(@PathVariable Long id, @RequestBody ManDto man) {
        log.info("Updating man by id: " + id);
        man.setId(id);
        ManDto manDto = manService.updateMan(man);
        log.info("Updated man: " + manDto);
        return new ResponseDto<>(manDto);
    }

    @PatchMapping("/{id}")
    public ResponseDto<ManDto> updateManParticularly(@PathVariable Long id, @RequestBody ManDto man) {
        log.info("Updating man by id: " + id);
        man.setId(id);
        ManDto manDto = manService.updateManParticularly(man);
        log.info("Updated man: " + manDto);
        return new ResponseDto<>(manDto);
    }

    @DeleteMapping("/{id}")
    public ResponseDto<ManDto> removeMan(@PathVariable Long id) {
        log.info("Deleting man by id: " + id);
        manService.removeMan(id);
        return new ResponseDto<>(ResponseStatus.SUCCESS, MAN_REMOVED_MESSAGE);
    }
}
