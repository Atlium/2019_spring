package com.atlium.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/")
public class HelloController {

    @GetMapping
    public String printHello(ModelMap modelMap) {
        modelMap.addAttribute("message", "Hello MVC!" + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()));
        return "hello";
    }
}
