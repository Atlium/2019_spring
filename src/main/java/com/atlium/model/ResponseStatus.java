package com.atlium.model;

public enum ResponseStatus {
    SUCCESS, ERROR;
}
