package com.atlium.error;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class RestResponseExceptionHandler {

    private static Logger log = Logger.getLogger(RestResponseExceptionHandler.class);

    @ExceptionHandler(Throwable.class)
    public void handleThrowable(Throwable e, HttpServletResponse response) {
        log.error(e.getMessage(), e);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        try {
            response.getWriter().append(e.getMessage()).flush();
        } catch (IOException io) {
            log.error("Failed to write error response: " + io.getMessage());
        }
    }

}
