package com.atlium.controller;

import com.atlium.dto.ManDto;
import com.atlium.dto.ResponseDto;
import com.atlium.model.ResponseStatus;
import com.atlium.service.ManService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/application-config.xml")
@ActiveProfiles("local")
public class ManControllerTest {

    private static final Logger log = Logger.getLogger(ManControllerTest.class);
    private static ObjectMapper objectMapper;
    private ManDto defaultManDto;

    private MockMvc mockMvc;

    @Mock
    private ManService manServiceMock;

    @BeforeClass
    public static void initClass() {
        objectMapper = new ObjectMapper();
        log.info("BEFORE CLASS PERFORMED");
    }

    @Before
    public void init() throws ParseException {
        defaultManDto = new ManDto(1L, "Vasya", "Pupkin", new SimpleDateFormat("dd.MM.yyyy").parse("10.10.1990"));
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(new ManController(manServiceMock)).build();
        log.info("BEFORE PERFORMED");
    }

    @Test
    public void testCreateMan() throws Exception {
        ManDto man = defaultManDto;
        when(manServiceMock.createMan(man)).thenReturn(man);
        mockMvc.perform(post("/mans")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(man)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(man))));
        verify(manServiceMock, times(1)).createMan(man);
        verifyNoMoreInteractions(manServiceMock);
    }

    @Test
    public void testFindAll() throws Exception {
        List<ManDto> mans = Collections.singletonList(defaultManDto);
        when(manServiceMock.findAllMans()).thenReturn(mans);
        mockMvc.perform(get("/mans"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(mans))));
        verify(manServiceMock, times(1)).findAllMans();
        verifyNoMoreInteractions(manServiceMock);
    }

    @Test
    public void testFind() throws Exception {
        ManDto man = defaultManDto;
        when(manServiceMock.findById(man.getId())).thenReturn(man);
        mockMvc.perform(get("/mans/{id}", man.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(man))));
        verify(manServiceMock, times(1)).findById(man.getId());
        verifyNoMoreInteractions(manServiceMock);
    }

    @Test
    public void testUpdate() throws Exception {
        ManDto man = defaultManDto;
        when(manServiceMock.updateMan(man)).thenReturn(man);
        mockMvc.perform(put("/mans/{id}", man.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(man)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(man))));
        verify(manServiceMock, times(1)).updateMan(man);
        verifyNoMoreInteractions(manServiceMock);
    }

    @Test
    public void testUpdateParticularly() throws Exception {
        ManDto man = defaultManDto;
        when(manServiceMock.updateManParticularly(man)).thenReturn(man);
        mockMvc.perform(patch("/mans/{id}", man.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(man)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(man))));
        verify(manServiceMock, times(1)).updateManParticularly(man);
        verifyNoMoreInteractions(manServiceMock);
    }

    @Test
    public void testRemove() throws Exception {
        ManDto man = defaultManDto;
        doNothing().when(manServiceMock).removeMan(man.getId());
        mockMvc.perform(delete("/mans/{id}", man.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(new ResponseDto<>(ResponseStatus.SUCCESS, ManController.MAN_REMOVED_MESSAGE))));
        verify(manServiceMock, times(1)).removeMan(man.getId());
        verifyNoMoreInteractions(manServiceMock);
    }
}
